variable "region" {
  description = "The AWS region to deploy resources in"
  type        = string
  default     = "us-west-2"
}

variable "vpc_cidr" {
  description = "CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "subnet_cidr" {
  description = "CIDR block for the public subnet"
  type        = string
  default     = "10.0.1.0/24"
}

variable "instance_type" {
  description = "The instance type for the bastion server"
  type        = string
  default     = "t2.micro"
}

variable "ami_id" {
  description = "The AMI ID for the bastion server"
  type        = string
  default     = "ami-0c55b159cbfafe1f0"
}

variable "key_name" {
  description = "The name of the SSH key pair to use for the bastion server"
  type        = string
  default     = "your_key_pair"
}

variable "allowed_ip" {
  description = "The IP address range that is allowed to access the bastion server"
  type        = string
  default     = "0.0.0.0/0"
}
