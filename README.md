# Документация по развертыванию Bastion Server с использованием Terraform

## Введение

Этот проект Terraform разворачивает bastion server (сервер-бастион) в AWS. Сервер-бастион используется для безопасного доступа к другим ресурсам в частной сети (VPC). В конфигурации создаются следующие ресурсы:

- VPC (Virtual Private Cloud)
- Публичная подсеть (Public Subnet)
- Интернет-шлюз (Internet Gateway)
- Таблица маршрутизации (Route Table)
- Группа безопасности (Security Group)
- Экземпляр EC2 для сервера-бастиона

## Структура проекта

Проект состоит из двух файлов:
- `variables.tf` - определение переменных
- `main.tf` - основная конфигурация

### Файл `variables.tf`

В этом файле определены переменные, которые используются в основной конфигурации.

```hcl
variable "region" {
  description = "Регион AWS для развертывания ресурсов"
  type        = string
  default     = "us-west-2"
}

variable "vpc_cidr" {
  description = "CIDR блок для VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "subnet_cidr" {
  description = "CIDR блок для публичной подсети"
  type        = string
  default     = "10.0.1.0/24"
}

variable "instance_type" {
  description = "Тип экземпляра для сервера-бастиона"
  type        = string
  default     = "t2.micro"
}

variable "ami_id" {
  description = "ID AMI для сервера-бастиона"
  type        = string
  default     = "ami-0c55b159cbfafe1f0"
}

variable "key_name" {
  description = "Имя пары ключей SSH для сервера-бастиона"
  type        = string
  default     = "your_key_pair"
}

variable "allowed_ip" {
  description = "Диапазон IP-адресов, которым разрешен доступ к серверу-бастиону"
  type        = string
  default     = "0.0.0.0/0"
}
```

### Файл `main.tf`

В этом файле определены основные ресурсы для развертывания.

```hcl
provider "aws" {
  region = var.region
}

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
}

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.subnet_cidr
  map_public_ip_on_launch = true
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

resource "aws_security_group" "bastion_sg" {
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.allowed_ip]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "bastion" {
  ami           = var.ami_id
  instance_type = var.instance_type
  subnet_id     = aws_subnet.public.id
  security_groups = [aws_security_group.bastion_sg.name]

  key_name = var.key_name
}

output "bastion_public_ip" {
  value = aws_instance.bastion.public_ip
}
```

## Шаги для развертывания

1. Установите Terraform, если он еще не установлен. Инструкции можно найти на [официальном сайте Terraform](https://www.terraform.io/downloads.html).

2. Создайте файл `variables.tf` и вставьте в него содержимое из секции `Файл variables.tf`.

3. Создайте файл `main.tf` и вставьте в него содержимое из секции `Файл main.tf`.

4. Инициализируйте проект Terraform. Для этого выполните команду в терминале:

```sh
terraform init
```

5. Примените конфигурацию для развертывания ресурсов в AWS:

```sh
terraform apply
```

6. Подтвердите выполнение, введя `yes` при появлении запроса.

## Заключение

После выполнения этих шагов Terraform создаст инфраструктуру в AWS, включая сервер-бастион. Для подключения к серверу-бастиону используйте публичный IP-адрес, который выводится в конце выполнения команды `terraform apply`.